using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Piece : MonoBehaviour
{
    public int ownerPlayer;
    public PieceType pieceType;

    public enum PieceType
    {
        Soldier,
        Captain
    }
}
