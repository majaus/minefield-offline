using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinTile : MonoBehaviour
{
    public bool active;
    public int ownerPlayer;
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Captain") && active)
        {
            if (ownerPlayer != other.gameObject.GetComponent<Piece>().ownerPlayer)
            {
                Manager.instance.Win(other.gameObject.GetComponent<Piece>().ownerPlayer);                   
            }
        }
    }
}
