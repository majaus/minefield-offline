using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mine : MonoBehaviour
{
    public int ownerPlayer;
    public bool fake;
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Soldier") || other.CompareTag("Captain"))
        {
            if (ownerPlayer != other.gameObject.GetComponent<Piece>().ownerPlayer)
            {
                if (fake)
                {
                    Destroy(gameObject);
                }
                else
                {                    
                    if (other.CompareTag("Captain"))
                    {
                        Manager.instance.Win(ownerPlayer);
                    }
                    Destroy(other.gameObject);
                    Destroy(gameObject);
                }                
            }
        }
    }

}

