using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Manager : MonoBehaviour
{
    public static Manager instance;
    public float cpuSpeed = 0.3f;
    public int[] mines = { 10, 10 };
    public int[] fakeMines = { 10, 10 };
    public int[] playerMoves = { 3, 3 };
    private int[] remainingMoves = { 3, 3 };
    public int rows = 13;
    public int cols = 13;

    public GameObject tilePrefab;
    public GameObject soldierPrefab;
    public GameObject captainPrefab;
    public GameObject minePrefab;
    public GameObject walkablePrefab;
    public GameObject walkabesCont;
    public GameObject minesSelectorCont;
    public GameObject mineSelectorPrefab;
    public Transform piecesTransform;
    
    private GameObject selectedPiece;
    private int currentPlayer = 1;       
    private Color[] playerColors = { Color.blue, Color.red };

    public TextMeshProUGUI currentPlayerText;
    public TextMeshProUGUI minesText;
    public TextMeshProUGUI fakeMinesText;
    public TextMeshProUGUI movesText;
    private bool setMine;
    private bool fakeMine;
    private int moves;
    public GameObject winScreen;
    public TextMeshProUGUI winText;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    void Start()
    {
        Array.Copy(playerMoves, remainingMoves, playerMoves.Length);
        GenerateBoard();
        PlacePieces();
    }
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector2 rayOrigin = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.zero);

            if (hit.collider != null)
            {
                GameObject hitObject = hit.collider.gameObject;

                if (hitObject.CompareTag("Tile"))
                {
                    if (selectedPiece != null)
                    {
                        Vector3 targetPosition = hitObject.transform.position;
                        moves = CalculateMoves(selectedPiece.transform.position, targetPosition);
                        if (moves <= remainingMoves[currentPlayer - 1])
                        {
                            // walk
                            if (!setMine)
                            {
                                MovePieze(targetPosition);                                
                            }
                            // set mine
                            else
                            {
                                if (moves > 1 || !IsValidMinePosition(targetPosition)) return;
                                PlaceMine(targetPosition);                                
                            }
                            // Switch player turn
                            if (remainingMoves[currentPlayer - 1] == 0 && !setMine)
                            {
                                SwitchTurn();                                
                            }
                            else
                            {
                                Vector3[] walkablePositions = CalculateWalkables();
                                InstantiateWalkables(walkablePositions);
                            }
                        }
                        else
                        {
                            Debug.Log("Not enough moves to reach that tile");
                        }
                    }
                }
                else if (hitObject.CompareTag("Soldier") || hitObject.CompareTag("Captain"))
                {
                    if (hitObject.GetComponent<Piece>().ownerPlayer == currentPlayer)
                    {
                        SelectPiece(hitObject);                        
                    }
                }
            }

        }
    }
    public void SwitchTurn()
    {
        if (selectedPiece)
        {
            selectedPiece.transform.GetChild(0).gameObject.SetActive(false);
        }        
        selectedPiece = null;
        currentPlayer = (currentPlayer == 1) ? 2 : 1;
        remainingMoves[currentPlayer - 1] = playerMoves[currentPlayer - 1];
        UpdateUI();
        RemoveWalkables();
        if (currentPlayer == 2)
        {
            StartCoroutine(MoveIA());
        }
    }
    public void SelectPiece(GameObject piece)
    {
        if (selectedPiece)
        {
            selectedPiece.transform.GetChild(0).gameObject.SetActive(false);
        }
        selectedPiece = piece;
        selectedPiece.transform.GetChild(0).gameObject.SetActive(true);
        Vector3[] walkablePositions = CalculateWalkables();
        InstantiateWalkables(walkablePositions);
        setMine = false;
    }
    public void PlaceMine(Vector3 targetPosition)
    {
        GameObject mine = Instantiate(minePrefab, new Vector3(targetPosition.x, targetPosition.y, 0), Quaternion.identity);
        mine.transform.SetParent(transform);
        mine.GetComponent<Mine>().ownerPlayer = currentPlayer;
        if (fakeMine)
        {
            if (currentPlayer == 1)
            {
                mine.gameObject.GetComponent<SpriteRenderer>().color = Color.grey;
            }
            else
            {
                mine.gameObject.GetComponent<SpriteRenderer>().color = Color.red;
            }
            mine.GetComponent<Mine>().fake = true;
            fakeMines[currentPlayer - 1]--;
        }
        else
        {
            if (currentPlayer == 1)
            {
                mine.gameObject.GetComponent<SpriteRenderer>().color = Color.black;
            }
            else
            {
                mine.gameObject.GetComponent<SpriteRenderer>().color = Color.red;
            }
            mines[currentPlayer - 1]--;
        }
        setMine = false;
        RemoveMinesSelector();       
        remainingMoves[currentPlayer - 1] -= 1;
        UpdateUI();
    }
    public void MovePieze(Vector3 targetPosition)
    {
        selectedPiece.transform.position = new Vector3(targetPosition.x, targetPosition.y, 0);
        remainingMoves[currentPlayer - 1] -= moves;
        UpdateUI();
    }
    public int CalculateMoves(Vector3 initialPosition, Vector3 finalPosition)
    {
        int x0 = (int)initialPosition.x;
        int y0 = (int)initialPosition.y;
        int x1 = (int)finalPosition.x;
        int y1 = (int)finalPosition.y;

        int dx = Mathf.Abs(x1 - x0);
        int dy = Mathf.Abs(y1 - y0);
        int sx = x0 < x1 ? 1 : -1;
        int sy = y0 < y1 ? 1 : -1;
        int err = dx - dy;
        int moves = 0;

        while (x0 != x1 || y0 != y1)
        {
            int e2 = err * 2;

            if (e2 > -dy)
            {
                err -= dy;
                x0 += sx;
            }

            if (e2 < dx)
            {
                err += dx;
                y0 += sy;
            }

            moves++;
        }
        return moves;
    }
    private void RemoveWalkables()
    {
        foreach (Transform walkable in walkabesCont.transform)
        {
            Destroy(walkable.gameObject);
        }
    }
    private void InstantiateWalkables(Vector3[] walkablePositions)
    {
        // Remove any existing walkables
        RemoveWalkables();
        RemoveMinesSelector();

        foreach (Vector3 position in walkablePositions)
        {
            GameObject walkable = Instantiate(walkablePrefab, position, Quaternion.identity);
            walkable.transform.SetParent(walkabesCont.transform);
        }
    }
    private Vector3[] CalculateWalkables()
    {
        List<Vector3> walkablePositions = new List<Vector3>();

        if (selectedPiece == null)
        {
            return walkablePositions.ToArray();
        }

        int movesLeft = remainingMoves[currentPlayer - 1];

        Vector3 piecePosition = selectedPiece.transform.position;

        for (int x = -movesLeft; x <= movesLeft; x++)
        {
            for (int y = -movesLeft; y <= movesLeft; y++)
            {
                Vector3 targetPosition = new Vector3(piecePosition.x + x, piecePosition.y + y, 0);

                if (IsValidWalkable(targetPosition))
                {
                    walkablePositions.Add(targetPosition);
                }
            }
        }
        return walkablePositions.ToArray();
    }
    private bool IsValidWalkable(Vector3 position)
    {
        if (position.x < 0 || position.x >= cols || position.y < 0 || position.y >= rows)
        {
            return false;
        }
        Collider2D collider = Physics2D.OverlapCircle(position, 0.2f);
        if (collider != null && ((collider.CompareTag("Soldier") && collider.gameObject != selectedPiece) || collider.CompareTag("Captain") || collider.CompareTag("Mine")))
        {
            return false;
        }
        return true;
    }
    private void RemoveMinesSelector()
    {
        foreach (Transform selector in minesSelectorCont.transform)
        {
            Destroy(selector.gameObject);
        }
    }
    private void InstantiateMinesSelector(Vector3[] minePositions)
    {
        RemoveMinesSelector();
        RemoveWalkables();

        foreach (Vector3 position in minePositions)
        {
            GameObject selector = Instantiate(mineSelectorPrefab, position, Quaternion.identity);
            selector.transform.SetParent(minesSelectorCont.transform);
        }
    }
    private bool IsValidMinePosition(Vector3 position)
    {
        if (position.x < 0 || position.x >= cols || position.y < 0 || position.y >= rows)
        {
            return false;
        }

        Collider2D collider = Physics2D.OverlapCircle(position, 0.2f);

        if (collider != null && collider.CompareTag("Tile"))
        {
            Collider2D[] adjacentColliders = Physics2D.OverlapCircleAll(position, 1f);
            foreach (Collider2D adjacentCollider in adjacentColliders)
            {
                if (adjacentCollider.CompareTag("Soldier") || adjacentCollider.CompareTag("Captain"))
                {
                    Piece adjacentPiece = adjacentCollider.GetComponent<Piece>();
                    if (adjacentPiece.ownerPlayer != currentPlayer)
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        return false;
    }
    private Vector3[] CalculateMinesSelector(Vector3 piecePosition)
    {
        List<Vector3> minePositions = new List<Vector3>();

        for (int x = -1; x <= 1; x++)
        {
            for (int y = -1; y <= 1; y++)
            {
                if (x == 0 && y == 0)
                {
                    continue;
                }

                Vector3 targetPosition = new Vector3(piecePosition.x + x, piecePosition.y + y, 0);

                if (IsValidMinePosition(targetPosition))
                {
                    minePositions.Add(targetPosition);
                }
            }
        }
        return minePositions.ToArray();
    }
    void UpdateUI()
    {
        minesText.text = "Mines: " + mines[currentPlayer - 1].ToString();
        currentPlayerText.text = "Player " + currentPlayer.ToString() + " Turn";
        movesText.text = remainingMoves[currentPlayer - 1].ToString();
        fakeMinesText.text = "Fake Mines: " + fakeMines[currentPlayer - 1].ToString();
    }
    public void SetMine(bool fake)
    {
        if (selectedPiece == null) return;

        if (!fake && mines[currentPlayer - 1] <= 0)
        {
            return;
        }
        if (fake && fakeMines[currentPlayer - 1] <= 0)
        {
            return;
        }

        if (selectedPiece.GetComponent<Piece>().pieceType == Piece.PieceType.Soldier)
        {
            fakeMine = fake;
            setMine = true;
            Vector3[] minePos = CalculateMinesSelector(selectedPiece.transform.position);
            InstantiateMinesSelector(minePos);
        }
        else
        {
            Debug.Log("Cannot set mine on a Captain");
        }
    }
    void GenerateBoard()
    {
        for (int row = 0; row < rows; row++)
        {
            for (int col = 0; col < cols; col++)
            {
                Vector3 position = new Vector3(col, row, 1);
                GameObject tile = Instantiate(tilePrefab, position, Quaternion.identity);
                tile.transform.SetParent(transform);
                if (row == 0)
                {
                    SpriteRenderer tileSprite = tile.GetComponent<SpriteRenderer>();
                    tileSprite.color = new Color(175 / 255f, 186 / 255f, 255 / 255f);
                    tile.GetComponent<WinTile>().active = true;
                    tile.GetComponent<WinTile>().ownerPlayer = 1;
                }
                else if (row == rows - 1)
                {
                    SpriteRenderer tileSprite = tile.GetComponent<SpriteRenderer>();
                    tileSprite.color = new Color(175 / 255f, 186 / 255f, 255 / 255f);
                    tile.GetComponent<WinTile>().active = true;
                    tile.GetComponent<WinTile>().ownerPlayer = 2;
                }
            }
        }
    }


    void PlacePieces()
    {
        // Place pieces for player 1
        int[] player1SoldierCols = { 3, 4, 5, 7, 8, 9 };
        int player1SoldierRow = 0;
        foreach (int col in player1SoldierCols)
        {
            Vector3 position = new Vector3(col, player1SoldierRow, 0);
            GameObject soldier = Instantiate(soldierPrefab, position, Quaternion.identity);
            soldier.GetComponent<SpriteRenderer>().color = playerColors[0];
            soldier.GetComponent<Piece>().ownerPlayer = 1;
            soldier.transform.SetParent(piecesTransform);
        }

        int player1CaptainCol = 6;
        int player1CaptainRow = 0;
        Vector3 player1CaptainPosition = new Vector3(player1CaptainCol, player1CaptainRow, 0);
        GameObject captain = Instantiate(captainPrefab, player1CaptainPosition, Quaternion.identity);
        captain.GetComponent<SpriteRenderer>().color = playerColors[0];
        captain.GetComponent<Piece>().ownerPlayer = 1;
        captain.transform.SetParent(piecesTransform);

        // Place pieces for player 2
        int[] player2SoldierCols = { 3, 4, 5, 7, 8, 9 };
        int player2SoldierRow = rows - 1;
        foreach (int col in player2SoldierCols)
        {
            Vector3 position = new Vector3(col, player2SoldierRow, 0);
            GameObject soldier = Instantiate(soldierPrefab, position, Quaternion.identity);
            soldier.GetComponent<SpriteRenderer>().color = playerColors[1]; // set red sprite color
            soldier.GetComponent<Piece>().ownerPlayer = 2;
            soldier.transform.SetParent(piecesTransform);
        }
        int player2CaptainCol = 6;
        int player2CaptainRow = rows - 1;
        Vector3 player2CaptainPosition = new Vector3(player2CaptainCol, player2CaptainRow, 0);
        GameObject redCaptain = Instantiate(captainPrefab, player2CaptainPosition, Quaternion.identity);
        redCaptain.GetComponent<SpriteRenderer>().color = playerColors[1]; // set red sprite color
        redCaptain.GetComponent<Piece>().ownerPlayer = 2;
        redCaptain.transform.SetParent(piecesTransform);
    }
    private IEnumerator MoveIA()
    {
        Piece[] pieces = piecesTransform.GetComponentsInChildren<Piece>();
        List<Piece> movablePieces = new List<Piece>();
        foreach (Piece piece in pieces)
        {
            if (piece.ownerPlayer == currentPlayer && remainingMoves[currentPlayer - 1] > 0)
            {
                movablePieces.Add(piece);
            }
        }
        if (movablePieces.Count == 0)
        {
            SwitchTurn();
            yield break;
        }
        Piece selectedPiece = movablePieces[UnityEngine.Random.Range(0, movablePieces.Count)];
        SelectPiece(selectedPiece.gameObject);
        yield return new WaitForSeconds(cpuSpeed);
        if (mines[currentPlayer - 1] > 0 && UnityEngine.Random.value < 0.3f)
        {
            setMine = true;
            Vector3[] minePositions = CalculateMinesSelector(selectedPiece.transform.position);
            if (minePositions.Length > 0)
            {
                Vector3 minePosition = minePositions[UnityEngine.Random.Range(0, minePositions.Length)];
                PlaceMine(minePosition);
                yield return new WaitForSeconds(cpuSpeed);
            }
        }
        else
        {
            Vector3[] walkablePositions = CalculateWalkables();
            if (walkablePositions.Length > 0)
            {
                Vector3 targetPosition;
                Vector3[] walkablePositionsSorted = GetLowestYPositions(walkablePositions);
                if (UnityEngine.Random.value < 0.1f)
                {
                    targetPosition = walkablePositionsSorted[UnityEngine.Random.Range(0, walkablePositionsSorted.Length)];
                }
                else
                {                    
                    targetPosition = walkablePositions[UnityEngine.Random.Range(0, walkablePositions.Length)];
                }
                moves = CalculateMoves(selectedPiece.transform.position, targetPosition);
                MovePieze(targetPosition);
                yield return new WaitForSeconds(cpuSpeed);
            }
        }        
        if (remainingMoves[currentPlayer - 1] > 0)
        {
            StartCoroutine(MoveIA());
        }
        else
        {
            SwitchTurn();
        }
    }
    private Vector3[] GetLowestYPositions(Vector3[] walkablePositions)
    {
        float minY = float.MaxValue;
        float maxY = float.MinValue;
        foreach (Vector3 position in walkablePositions)
        {
            if (position.y < minY)
            {
                minY = position.y;
            }
            if (position.y > maxY)
            {
                maxY = position.y;
            }
        }
        List<Vector3> lowestYPositions = new List<Vector3>();
        foreach (Vector3 position in walkablePositions)
        {
            if (position.y == minY)
            {
                lowestYPositions.Add(position);
            }
        }
        Vector3[] result = new Vector3[lowestYPositions.Count];
        lowestYPositions.CopyTo(result, 0);
        return result;
    }
    public void Win(int player)
    {
        StopAllCoroutines();        
        winScreen.SetActive(true);
        winText.text = "Player: " + player + " Win!!";
    }
    public void PlayAgain()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}